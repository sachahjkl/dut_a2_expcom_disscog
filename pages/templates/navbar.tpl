<nav class="navbar navbar-expand-lg navbar-light bg-light p-1 " style="border-bottom: 5px solid black ">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<div class="container">
			<ul class="navbar-nav mr-auto align-items-center">
				<li class="nav-item">
					<a class="navbar-brand" href="./index.html"><img src="./img/iconHeader.png" width="50" alt="logo_cog"></a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./index.html">Accueil</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/definitions/content.html">Définitions</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/histoire/content.html">Histoire</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/analyse/content.html">Analyse du phénomène</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/etudes&experiences/content.html">Etudes et expériences</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/solutions/content.html">Solutions</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/comparaisons/content.html">Comparaisons</a>
				</li>
				<li class="nav-item">
					<a class="nav-link text-center" href="./pages/bibliographie/content.html">Bibliographie</a>
				</li>
			</ul>
		</div>
	</div>
</nav>